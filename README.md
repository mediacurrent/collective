# Collective Directory Install Profile

The Collective Directory distribution is for organizations dedicated to preserving historical collective.

### Base features

The Collective Directory leverages Paragraphs for easy page-building and many other features provided by the Rain distribution.
Though the other features include the following:

* Events
* News
* Social Media
* Organization Directory
* Site Search
* Photo Galleries

### Basic Installation

* Follow quick start guide if you want to get started as fast as possible. If not use basic installation and configure to your own needs.
* The intended usage of this project is to be pulled into a full [Drupal composer-based project](https://bitbucket.org/mediacurrent/drupal-project/).
* See [MIS Rain Vagrant](https://bitbucket.org/mediacurrent/mis_rain_vagrant/src) project for example usage. Note that this project uses Rain as a base profile.
* To install as a stand-alone install, use the command: ```drush si -y collective```
* See [Drupal Sub-profiles thread](https://www.drupal.org/node/1356276) for details around using an install profile as a "subprofile."
* This profile can also be forked easily by renaming files and replacing project name with custom project name.

### Usage

The Collective Directory install profile is primarily used to help with Drupal core/contrib dependency management and provide configuration that can be easily modified.
It is not necessarily recommended for applications using the profile to import downstream configuration updates. Configuration provided is intended as a
"starter" when the install profile is run for the very first time. Once configuration has been committed to git, downstream updates will not effect
the Drupal 8 application (by design). Downstream Composer updates however can be extremely beneficial as they reduce the maintenance burden for application owners to manage
dozens of dependencies provided by this install profile. Pulling down these updates is recommended.

### Quickstart Guide

## Requirements
Install [Composer](https://getcomposer.org/)
Install [ddev](https://www.ddev.com/)

## Create Drupal 8 project using composer
`composer create-project mediacurrent/drupal-project:8.x-dev collective`

## Modify composer.json and add custom OG package in repository section
```
  {
      "type":"package",
      "package": {
        "name": "drupal/og",
        "version":"1.0",
        "type":"drupal-module",
        "source": {
          "url": "https://github.com/damienmckenna/og.git",
          "type": "git",
          "reference":"160-243-231"
        }
      }
  }
```

## Install Collective Profile via composer
```
cd collective
composer require mediacurrent/collective
```

## Configure ddev for local development
```
ddev config
```

* Leave the project name set to the name of your project directory (collective).
* Leave the docroot location set to 'web'.
* Leave the project type set to 'drupal8'.

Start ddev
`ddev start`

## Build site using Collective profile and drush
`ddev . drush @self site-install collective -y --site-name=Collective --site-mail=admin@example.com --account-mail=admin@example.com --account-name=admin --account-pass=admin`

* Once site is done installing you can login at `http://collective.ddev.site/user`


### Making configuration updates to this project

* One aspect of making updates to this project is that its important to remove uuids/hashes from configuration. The base script below will do this for you.
```
#!/bin/bash
FILES=config/install/*
for f in $FILES
do
  echo "Processing $f file..."
  sed -i '' '/^uuid:/d' $f
  sed -i '' '/^\_core:/d' $f
  sed -i '' '/^  default_config_hash:/d' $f
done
```
### Contribution

* [Collective Directory drupal.org project page](https://www.drupal.org/project/collective)
* [Collective Directory issue queue](https://www.drupal.org/project/issues/collective)
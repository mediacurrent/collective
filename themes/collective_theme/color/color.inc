<?php

/**
 * @file
 * Lists available colors and color schemes for the collective theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'top' => t('Top header background'),
    'bottom' => t('Main menu background'),
    'base' => t('Main background'),
    'breadcrumbs' => t('Breadcrumb background'),
    'button_bg' => t('Button background'),
    'link' => t('Link color'),
    'text' => t('Text color'),
    'footer' => t('Footer background'),
    'bottom_footer' => t('Bottom Footer background'),
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Fashionably Classic (default)'),
      'colors' => [
        'top' => '#5c5b61',
        'bottom' => '#557a95',
        'base' => '#fcfcfc',
        'breadcrumbs' => '#b1a096',
        'button_bg' => '#f5ae7a',
        'link' => '#0071b3',
        'text' => '#292929',
        'footer' => '#557a95',
        'bottom_footer' => '#5c5b61',
      ],
    ],
    'eclectic_melange' => [
      'title' => t('Eclectic Melange'),
      'colors' => [
        'top' => '#484854',
        'bottom' => '#854f37',
        'base' => '#f5f5f5',
        'breadcrumbs' => '#86b3d2',
        'button_bg' => '#86b3d2',
        'link' => '#033649',
        'text' => '#292929',
        'footer' => '#854f37',
        'bottom_footer' => '#484854',
      ],
    ],
    'understated_sophisicate' => [
      'title' => t('Understated Sophisicate'),
      'colors' => [
        'top' => '#191919',
        'bottom' => '#699874',
        'base' => '#ededed',
        'breadcrumbs' => '#806257',
        'button_bg' => '#21718d',
        'link' => '#21718d',
        'text' => '#292929',
        'footer' => '#699874',
        'bottom_footer' => '#292929',
      ],
    ],
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'dist/css/breadcrumb.css',
    'dist/css/button.css',
    'dist/css/global.css',
    'dist/css/header.css',
    'dist/css/main-menu.css',
    'dist/css/search-box.css',
    'dist/css/user-account-nav.css',
  ],

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],

  // Gradient definitions.
  'gradients' => [
    [
      // (x, y, width, height).
      'dimension' => [0, 0, 0, 0],
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => ['top', 'bottom'],
    ],
  ],

  // Preview files.
  'preview_library' => 'collective_theme/color.preview',
  'preview_html' => 'color/preview.html',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
        'logo' => theme_get_setting('logo.url', 'collective'),
      ],
    ],
  ],
];

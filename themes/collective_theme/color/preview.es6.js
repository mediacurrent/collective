/**
 * @file
 * Preview for the collective theme.
 */
'use strict';

(function($, Drupal, drupalSettings) {
  Drupal.color = {
    logoChanged: false,
    callback(context, settings, $form) {
      const $colorPreview = $form.find('.color-preview');
      const $colorPalette = $form.find('.js-color-palette');

      // Solid background.
      $colorPreview.css(
        'backgroundColor',
        $colorPalette.find('input[name="palette[background]"]').val()
      );

      // Main Menu
      const $colorPreviewBlock = $colorPreview.find('.color-preview-main-menu');
      $colorPreviewBlock.css(
        'backgroundColor',
        $colorPalette.find('input[name="palette[bottom]"]').val()
      );

      // Top Menu background.
      $colorPreview
        .find('.color-preview-top-menu')
        .css(
          'background-color',
          $colorPalette.find('input[name="palette[top]"]').val()
        );

      // Text preview.
      $colorPreview
        .find('color-preview-content p')
        .css('color', $colorPalette.find('input[name="palette[text]"]').val());

      // Link preview
      $colorPreview
        .find('.color-preview-content a')
        .css('color', $colorPalette.find('input[name="palette[link]"]').val());

      // Main Footer background.
      $colorPreview
        .find('.color-preview-footer')
        .css(
          'background-color',
          $colorPalette.find('input[name="palette[footer]"]').val()
        );

      // Bottom Footer background.
      $colorPreview
        .find('.color-preview-footer-bottom')
        .css(
          'background-color',
          $colorPalette.find('input[name="palette[bottom_footer]"]').val()
        );
    }
  };
})(jQuery, Drupal, drupalSettings);

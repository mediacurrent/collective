'use strict';

!function (document, Drupal, $) {
  Drupal.behaviors.mobileMenu = {
    attach: function attach(context) {
      $('.main-menu__navicon', context).click(function () {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
      });
    }
  };
}(document, Drupal, jQuery);
//# sourceMappingURL=main-menu.js.map

'use strict';

!function (document, Drupal, $) {
  Drupal.behaviors.organizationInformation = {
    attach: function attach(context) {
      $('.organization-info__toggle', context).once().click(function () {
        if (parseInt($(window).width()) < 768) {
          // In the raw HTML output the container is the very next DOM item
          // after the toggle button, so this should be safe to do.
          var $infoContainer = $(this).next();
          var $toggleIcon = $('.organization-info__toggle-icon', this);
          var expandedState = $infoContainer.attr('aria-expanded') === 'true' ? 'false' : 'true';
          var hiddenState = $infoContainer.attr('aria-hidden') === 'false' ? 'false' : 'true';
          $infoContainer.attr('aria-expanded', expandedState);
          $infoContainer.attr('aria-hidden', hiddenState);
          $infoContainer.toggle();
          $toggleIcon.toggleClass('open closed');
        }
      });
    }
  };
}(document, Drupal, jQuery);
//# sourceMappingURL=organization-information.js.map

'use strict';

!function (document, Drupal, $) {
  Drupal.behaviors.organizationTeaser = {
    attach: function attach(context) {
      $('.organization-teaser__contact-toggle', context).once().click(function () {
        // In the raw HTML output the container is the very next DOM item
        // after the toggle button, so this should be safe to do.
        var $contactContainer = $(this).next();
        var $toggleIcon = $('.organization-teaser__toggle-icon', this);
        var $toggleTxt = $('.organization-teaser__toggle-text', this);
        var expandedState = $contactContainer.attr('aria-expanded') === 'false' ? 'true' : 'false';
        var hiddenState = $contactContainer.attr('aria-hidden') === 'true' ? 'false' : 'true';
        var toggleStateTxt = expandedState === 'true' ? Drupal.t('Hide contact information') : Drupal.t('Show contact information');
        $toggleTxt.text(toggleStateTxt);
        $contactContainer.toggle();
        $toggleIcon.toggleClass('open closed');
        $contactContainer.attr('aria-expanded', expandedState);
        $contactContainer.attr('aria-hidden', hiddenState);
      });
    }
  };
}(document, Drupal, jQuery);
//# sourceMappingURL=organization-teaser.js.map

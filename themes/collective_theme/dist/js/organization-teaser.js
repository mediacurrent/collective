'use strict';

!function (document, Drupal, $) {
  Drupal.behaviors.organizationTeaser = {
    attach: function attach(context) {
      $('.organization-teaser__contact-toggle', context).once().click(function () {
        // In the raw HTML output the container is the very next DOM item
        // after the toggle button, so this should be safe to do.
        var $contactContainer = $(this).next();
        var $toggleIcon = $('.organization-teaser__toggle-icon', this);
        var $toggleTxt = $('.organization-teaser__toggle-text', this);
        var expandedState = $contactContainer.attr('aria-expanded') === 'false' ? 'true' : 'false';
        var hiddenState = $contactContainer.attr('aria-hidden') === 'true' ? 'false' : 'true';
        var toggleStateTxt = expandedState === 'true' ? Drupal.t('Hide contact information') : Drupal.t('Show contact information');
        $toggleTxt.text(toggleStateTxt);
        $contactContainer.toggle();
        $toggleIcon.toggleClass('open closed');
        $contactContainer.attr('aria-expanded', expandedState);
        $contactContainer.attr('aria-hidden', hiddenState);
      });
    }
  };
}(document, Drupal, jQuery);
//# sourceMappingURL=organization-teaser.js.map

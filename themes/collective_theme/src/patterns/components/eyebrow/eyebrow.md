---
title: Eyebrow Component
---
### Description

- The *Eyebrow* component is to be used as a small heading that is displayed above a main, larger heading.
- This component is in the `collective_theme` Pattern Lab for demonstration only.
- The source code for the *Eyebrow* component lives in the parent theme, `rain_theme/src/patterns/components/eyebrow`.
- All styles are included from `rain_theme/src/patterns/components/eyebrow/eyebrow.scss` and any styles added to `collective_theme/src/patterns/components/eyebrow/eyebrow.scss` are overrides specific to `collective_theme`.

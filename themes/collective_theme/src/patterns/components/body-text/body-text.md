---
title: Body Text Component
---
### Description

- The *Body Text* component is to be used with the *Text* Paragraph type in Drupal.
- This component is in the `collective_theme` Pattern Lab for demonstration only.
- The source code for the *Body Text* component lives in the parent theme, `rain_theme/src/patterns/components/body-text`.
- All styles are included from `rain_theme/src/patterns/components/body-text/body-text.scss` and any styles added to `collective_theme/src/patterns/components/body-text/body-text.scss` are overrides specific to `collective_theme`.

---
title: Heading Component
---
### Description

- The *Heading* component is to be used to display a heading in other components. This helps keep all heading size & styles consistent.
- This component is in the `collective_theme` Pattern Lab for demonstration only.
- The source code for the *Heading* component lives in the parent theme, `rain_theme/src/patterns/components/heading`.
- All styles are included from `rain_theme/src/patterns/components/heading/heading.scss` and any styles added to `collective_theme/src/patterns/components/heading/heading.scss` are overrides specific to `collective_theme`.

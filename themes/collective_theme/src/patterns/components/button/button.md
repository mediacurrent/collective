---
title: Button Component
---
### Description

- The *Button* component is to be used in a variety of other components as a CTA Link.
- This component is in the `collective_theme` Pattern Lab for demonstration only.
- The source code for the *Button* component lives in the parent theme, `rain_theme`.
- All styles are included from `rain_theme` and any styles added to `button.scss` are overrides specific to `collective_theme`.

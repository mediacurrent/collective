---
title: Card Component
---
### Description

- The *Card* component is to be used as a Call to Action that includes an image, summary, and link to content it is promoting.
- The *Card* component has 2 variants:
  - Wide: Image on left
  - Wide: Image on Right
- This component is in the `collective_theme` Pattern Lab for demonstration only.
- The source code for the *Card* component lives in the parent theme, `rain_theme`.
- All styles are included from `rain_theme` and any styles added to `card.scss` are overrides specific to `collective_theme`.
